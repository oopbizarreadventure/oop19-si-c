﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace virtualworld
{
    class GenericStage : Stage
    {
        //private EntitySpawner spawner;
        private bool running;
        private bool alive;
        private WaveGenerator waves = new WaveGenerator();
        private IWave currentWave;
        //private VirtualMapPrototype<UUIDActor, UUIDProjectile> map = new VirtualMapPrototype<>(ResourceManagerAlpha.getIstance().getSettingsAsObject().getHeight() - (64 * (ResourceManagerAlpha.getIstance().getSettingsAsObject().getHeight() / 540)), ResourceManagerAlpha.getIstance().getSettingsAsObject().getWidth());
        //private EnemyFormationGenerator generator = new EnemyFormationGeneratorImpl((int)this.getMap().getWidth(), (int)this.getMap().getHeigth());
        private bool paused = false;
        //private Difficulty diff = ControlsMenuController.getDiff();
        //private PlayerShip player = new PlayerShip(0, 0);
        //private int score = 0;

        public void start()
        {
            currentWave = waves.next();
            //rimosso istruzioni che contengono dipendenze a elementi non istanziabili
            running = true;
            alive = true;
            run();
        }

        public void run()
        {
            while(running && alive)
            {
                while (paused)
                {
                    Thread.Sleep(1000);
                }
                if (currentWave.IsEnded())
                {
                    if (waves.HasNext())
                    {
                        currentWave = waves.next();
                    }
                }
                else
                {
                    stop();
                }
            }
        }

        public IWave getWave()
        {
            return currentWave;
        }

        public bool isEnded()
        {
            return !alive;
        }

        public bool isPaused()
        {
            return paused;
        }

        public void pause()
        {
            paused = true;
        }

        public void resume()
        {
            paused = false;
        }

        public void stop()
        {
            alive = false;
            running = false;
        }
    }
}
