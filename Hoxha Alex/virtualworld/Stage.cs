﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace virtualworld
{
    interface Stage
    {
        void start();
        void stop();
        bool isEnded();
        void pause();
        void resume();
        bool isPaused();
        IWave getWave();
        //VirtualMap<UUIDActor, UUIDProjectile> getMap();
        //PlayerShip getplayer();
        //int getScore();

        //void setSpawner(EntitySpawner spawner); riferimenti a classi non mie
    }
}
