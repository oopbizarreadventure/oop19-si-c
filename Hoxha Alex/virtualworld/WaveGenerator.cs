﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace virtualworld
{
    class WaveGenerator
    {
        private int current = 1;
        private readonly int numWaves = 15;
        //private LevelParametizer parametizer = new LevelParametizerImpl(ControlsMenuController.getDiff(), numWaves, false);


        public bool HasNext()
        {
            return !(current == numWaves);
        }

        public IWave next()
        {
            current++;
            return generate();
        }

        public IWave generate()
        {
            return new WaveImpl();

        }

    }
}
