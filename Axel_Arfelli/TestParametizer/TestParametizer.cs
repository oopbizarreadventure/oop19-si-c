﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using model;

    [TestClass]
    internal class TestParametizer
	{
		private bool InstanceFieldsInitialized = false;

		private const int WAVE_NUM = 5;

		private WaveInfo_Difficulty diff = WaveInfo_Difficulty.EASY;
		private LevelData levelNormal;
		private LevelData levelEndless;
		private LevelParametizerImpl lp;

		public TestParametizer()
		{
			if (!InstanceFieldsInitialized)
			{
				InitializeInstanceFields();
				InstanceFieldsInitialized = true;
			}
		}

		private void InitializeInstanceFields()
		{
			levelNormal = new LevelDataImpl(diff, WAVE_NUM, 0, 0, false);
			levelEndless = new LevelDataImpl(diff, 0, 0, 0, true);
		}		

		[TestMethod]
		[ExpectedException(typeof(InvalidOperationException))]
		public virtual void TestLevelEasy()
		{
			Console.WriteLine(">>TEST = Normal Level - Easy");
			lp = new LevelParametizerImpl(diff, WAVE_NUM, false);
			// normal procedure
			WaveInfo newWave = lp.newWave();
			Assert.AreEqual(WaveInfo_Difficulty.EASY, newWave.Difficulty);
			Assert.AreEqual(1, newWave.Number);
			int hp = newWave.elaborateInitialHP(WAVE_NUM);
			int dmg = newWave.elaborateInitialDMG(WAVE_NUM / WAVE_NUM);
			// exception
			for (int i = 1; i < WAVE_NUM; i++)
			{
				newWave = lp.newWave();
			}
            _ = Assert.ThrowsException < InvalidOperationException > (() =>
              {
                  lp.newWave();
              });
			Console.WriteLine(" ");
		}

		[TestMethod]
		public virtual void testData()
		{
			Console.WriteLine(">>TEST = Level Data - Load and Save");
			lp = new LevelParametizerImpl(levelNormal);
			// normal procedure
			WaveInfo newWave = lp.newWave();
			Assert.AreEqual(WaveInfo_Difficulty.EASY, newWave.Difficulty);
			Assert.AreEqual(1, newWave.Number);
			int hp = newWave.elaborateInitialHP(WAVE_NUM);
			int dmg = newWave.elaborateInitialDMG(WAVE_NUM / WAVE_NUM);
			// contextual elaborated values
			for (int i = 1; i < WAVE_NUM; i++)
			{
				newWave = lp.newWave();
				hp = newWave.elaborateInitialHP(WAVE_NUM);
				dmg = newWave.elaborateInitialDMG(WAVE_NUM / WAVE_NUM);
				Console.WriteLine("HP: " + hp + " DMG: " + dmg);
			}
			// closing procedure
			levelNormal = lp.closeLevel();
			Console.WriteLine(levelNormal.ToString());
			Console.WriteLine(" ");
		}

		[TestMethod]
		public virtual void testEndless()
		{
			Console.WriteLine(">>TEST = Endless Mode");
			lp = new LevelParametizerImpl(levelEndless);
			// normal procedure
			WaveInfo newWave = lp.newWave();
			Assert.AreEqual(WaveInfo_Difficulty.EASY, newWave.Difficulty);
			Assert.AreEqual(1, newWave.Number);
			int hp = newWave.elaborateInitialHP(WAVE_NUM);
			int dmg = newWave.elaborateInitialDMG(WAVE_NUM / WAVE_NUM);
			// no exception
			for (int i = 1; i < WAVE_NUM * 2; i++)
			{
				newWave = lp.newWave();
			}
			hp = newWave.elaborateInitialHP(WAVE_NUM);
			dmg = newWave.elaborateInitialDMG(WAVE_NUM / WAVE_NUM);
			Console.WriteLine("HP: " + hp + " DMG: " + dmg);
			// closing procedure
			levelNormal = lp.closeLevel();
			Console.WriteLine(levelNormal.ToString());
			Console.WriteLine(" ");
		}

	}