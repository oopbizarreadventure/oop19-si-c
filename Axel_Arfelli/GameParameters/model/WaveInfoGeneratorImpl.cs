﻿namespace model
{

	/// <summary>
	/// Implementation of the Wave Generator.
	/// It is included in and called through LevelParametizerImpl.
	/// </summary>
	public class WaveInfoGeneratorImpl : WaveInfoGenerator
	{

		private readonly WaveInfo_Difficulty waveDiff;

		/// <summary>
		/// Constructor for WaveGenerator.
		/// Must be initialized with Difficulty for parameterization.
		/// </summary>
		/// <param name="waveDiff"> = Difficulty used by every WaveInfo it will generate </param>
		public WaveInfoGeneratorImpl(WaveInfo_Difficulty waveDiff)
		{
			this.waveDiff = waveDiff;
		}

		public WaveInfo loadWaveStandard(int stageNUM)
		{
			return new WaveInfoAnonymousInnerClass(this, stageNUM);
		}

		private class WaveInfoAnonymousInnerClass : WaveInfo
		{
			private readonly WaveInfoGeneratorImpl outerInstance;

			private int stageNUM;

			public WaveInfoAnonymousInnerClass(WaveInfoGeneratorImpl outerInstance, int stageNUM)
			{
				this.outerInstance = outerInstance;
				this.stageNUM = stageNUM;
			}


			public int elaborateInitialHP(int baseValue)
			{
				return (int) baseValue * (outerInstance.waveDiff.EnemyInitialLEVEL + stageNUM);
			}

			public int elaborateInitialDMG(int baseValue)
			{
				return (int) baseValue * outerInstance.waveDiff.EnemyMULTIPLIER + stageNUM;
			}

			public int elaborateScore(int enemyTier)
			{
				return (int) enemyTier * outerInstance.waveDiff.EnemyMULTIPLIER;
			}

			public WaveInfo_Difficulty Difficulty
			{
				get
				{
					return outerInstance.waveDiff;
				}
			}

			public int Number
			{
				get
				{
					return stageNUM;
				}
			}
		}

		public WaveInfo_Difficulty Difficulty
		{
			get
			{
				return this.waveDiff;
			}
		}

	}

}