﻿namespace model
{

	/// <summary>
	/// Generator used to create Waves and elaborate local parameters and values.
	/// </summary>
	public interface WaveInfoGenerator
	{

		/// <summary>
		/// Returns a new WaveInfo tagged with its own number.
		/// Number is used for parameterization.
		/// </summary>
		/// <param name="waveNUM"> = number of the Wave </param>
		/// <returns> a brand new WaveInfo </returns>
		WaveInfo loadWaveStandard(int waveNUM);

		/// <summary>
		/// Returns Difficulty used to initialize the generator.
		/// </summary>
		/// <returns> WaveGenerator Difficulty </returns>
		WaveInfo_Difficulty Difficulty {get;}

	}

}