﻿namespace model
{

	/// <summary>
	/// This Class provides a data structure for storing
	/// information about a Level and its waves.
	/// </summary>
	public interface LevelData
	{

		/// <summary>
		/// Returns the Difficulty used to initialize Level.
		/// </summary>
		/// <returns> Level Difficulty </returns>
		WaveInfo_Difficulty LevelDifficulty {get;}

		/// <summary>
		/// Returns the total amount of Waves this Level will reach.
		/// </summary>
		/// <returns> Total amount of Waves </returns>
		int TotalWaves {get;}

		/// <summary>
		/// Returns the number of the last wave reached and completed.
		/// </summary>
		/// <returns> Number of Last Wave Completed </returns>
		int LastWaveNumber {get;set;}


		/// <summary>
		/// This method will increase by 1 the number of waves completed.
		/// </summary>
		void increaseLastWaveNumber();

		/// <summary>
		/// This method will give the amount of score points accumulated.
		/// </summary>
		/// <returns> Total amount of Score Points </returns>
		int TotalScore {get;set;}


		/// <summary>
		/// This method will tell if the Level is in Endless Mode.
		/// </summary>
		/// <returns> true if Endless Mode is active, false otherwise </returns>
		bool Endless {get;}

	}

}