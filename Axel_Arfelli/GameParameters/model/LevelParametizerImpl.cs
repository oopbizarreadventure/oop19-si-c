﻿using System;

namespace model
{

	/// <summary>
	/// This class is used to deliver new instances of WaveInfo
	/// and generic level-related information whenever required.
	/// It can also return the LevelDataImpl in order to resume the 
	/// game-play later.
	/// </summary>
	public class LevelParametizerImpl : LevelParametizer
	{

		/* BackBone Logic components */
		private WaveInfoGenerator waveGenerator;
		/* Data for Wave Parameters */
		private LevelData levelData;

		/// <summary>
		/// Generic constructor for the Parametizer.
		/// </summary>
		/// <param name="levelDiff"> = Difficulty of Level </param>
		/// <param name="totalWaves"> = Total number of waves (if Endless, make it 0) </param>
		/// <param name="savedWave"> = Number of ongoing Wave </param>
		/// <param name="savedScore"> = Total score reached </param>
		/// <param name="endlessMode"> = If it's an Endless Mode Level </param>
		public LevelParametizerImpl(WaveInfo_Difficulty levelDiff, int totalWaves, int savedWave, int savedScore, bool endlessMode)
		{
			// level data
			this.levelData = new LevelDataImpl(levelDiff, totalWaves, savedWave, savedScore, endlessMode);
			// wave generator
			this.waveGenerator = new WaveInfoGeneratorImpl(levelDiff);
		}
		/// <summary>
		/// Standard constructor for new Level Parametizer.
		/// </summary>
		/// <param name="levelDiff"> = Difficulty of Level </param>
		/// <param name="totalWaves"> = Total number of waves (if Endless, make it 0) </param>
		/// <param name="endlessMode"> = If it's an Endless Mode Level </param>
		public LevelParametizerImpl(WaveInfo_Difficulty levelDiff, int totalWaves, bool endlessMode) : this(levelDiff, totalWaves, 0, 0, endlessMode)
		{
		}
		/// <summary>
		/// Constructor for saved Level Data to resume.
		/// </summary>
		/// <param name="savedLevelData"> = Saved Level Data to resume game-play </param>
		public LevelParametizerImpl(LevelData savedLevelData)
		{
			// level data
			this.levelData = savedLevelData;
			// wave generator
			this.waveGenerator = new WaveInfoGeneratorImpl(savedLevelData.LevelDifficulty);
		}

		/// <summary>
		/// Calls a new WaveInfo from the generator, if available;
		/// otherwise throws an Exception.
		/// </summary>
		/// <returns> a new WaveInfo with LastWaveNumber </returns>
		/// <exception cref="InvalidOperationException"> </exception>
		public WaveInfo newWave()
		{
			if (hasNext())
			{
				levelData.increaseLastWaveNumber();
				return waveGenerator.loadWaveStandard(this.levelData.LastWaveNumber);
			}
			else
			{
				throw new InvalidOperationException();
			}
		}

		/// <summary>
		/// Checks if a new Wave can be launched.
		/// </summary>
		/// <returns> TRUE if Total Waves >= Last Wave + 1,
		///          TRUE if is an Endless Level,
		///          FALSE otherwise </returns>
		public bool hasNext()
		{
			return ((this.levelData.TotalWaves >= this.levelData.LastWaveNumber + 1) || this.levelData.Endless);
		}

		/// <summary>
		/// When the game is to be closed, use this method to recover
		/// LevelData (Implementation) and save for later resuming.
		/// </summary>
		/// <returns> LevelData relative to ongoing game </returns>
		public LevelData closeLevel()
		{
			this.levelData.LastWaveNumber = this.levelData.LastWaveNumber - 1;
			return this.levelData;
		}

		/// <summary>
		/// Prints info of LevelParametizer for Debugging.
		/// Testing purpose only.
		/// </summary>
		public void debugPrintLevelData()
		{
			Console.WriteLine(this.levelData.ToString());
		}

		public WaveInfo_Difficulty LevelDifficulty
		{
			get
			{
				return this.levelData.LevelDifficulty;
			}
		}

	}

}