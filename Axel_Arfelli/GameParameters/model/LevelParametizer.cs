﻿namespace model
{

	/// <summary>
	/// This class is used to deliver new instances of WaveInfo
	/// and generic level-related information whenever required.
	/// It can also return the LevelDataImpl in order to resume the 
	/// game-play later.
	/// </summary>
	public interface LevelParametizer
	{

		/// <summary>
		/// Calls a new WaveInfo from the generator, if available;
		/// otherwise throws an Exception.
		/// </summary>
		/// <returns> a new WaveInfo with LastWaveNumber </returns>
		/// <exception cref="NoSuchElementException"> </exception>
		WaveInfo newWave();

		/// <summary>
		/// Checks if a new Wave can be launched.
		/// </summary>
		/// <returns> TRUE if Total Waves >= Last Wave + 1,
		///          TRUE if is an Endless Level,
		///          FALSE otherwise </returns>
		bool hasNext();

		/// <summary>
		/// When the game is to be closed, use this method to recover
		/// LevelDataImpl and save for later resuming.
		/// </summary>
		/// <returns> LevelData relative to ongoing game </returns>
		LevelData closeLevel();

		/// <summary>
		/// Returns the Difficulty used to initialize Level.
		/// </summary>
		/// <returns> Level Difficulty </returns>
		WaveInfo_Difficulty LevelDifficulty {get;}

	}
}