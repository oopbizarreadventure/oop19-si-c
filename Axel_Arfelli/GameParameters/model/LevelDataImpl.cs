﻿using System;

namespace model
{

	/// <summary>
	/// This Class provides a data structure for storing
	/// information about a Level and its waves.
	/// 
	/// </summary>
	[Serializable]
	public class LevelDataImpl : LevelData
	{

		private const long serialVersionUID = 1L;

		private WaveInfo_Difficulty levelDifficulty;
		private int totalWaves;
		private int lastWaveNumber;
		private int totalScore;
		private bool endlessMode;

		/// <summary>
		/// Constructor for LevelDataImpl structure.
		/// </summary>
		/// <param name="levelDifficulty"> = Difficulty of Level </param>
		/// <param name="totalWaves"> = Total number of waves (if Endless, make it 0) </param>
		/// <param name="lastWaveNumber"> = Number of ongoing Wave </param>
		/// <param name="totalScore"> = Total score reached </param>
		/// <param name="endlessMode"> = If it's an Endless Mode Level </param>
		public LevelDataImpl(WaveInfo_Difficulty levelDifficulty, int totalWaves, int lastWaveNumber, int totalScore, bool endlessMode)
		{
			this.levelDifficulty = levelDifficulty;
			this.totalWaves = totalWaves;
			this.lastWaveNumber = lastWaveNumber;
			this.totalScore = totalScore;
			this.totalScore = totalScore;
			this.endlessMode = endlessMode;
		}

		public WaveInfo_Difficulty LevelDifficulty
		{
			get
			{
				return this.levelDifficulty;
			}
		}
		public int TotalWaves
		{
			get
			{
				return this.totalWaves;
			}
		}
		public int LastWaveNumber
		{
			get
			{
				return this.lastWaveNumber;
			}
			set
			{
				this.lastWaveNumber = value;
			}
		}
		public void increaseLastWaveNumber()
		{
			this.lastWaveNumber++;
		}
		public int TotalScore
		{
			get
			{
				return this.totalScore;
			}
			set
			{
				this.totalScore = value;
			}
		}
		public bool Endless
		{
			get
			{
				return this.endlessMode;
			}
		}
		public override sealed string ToString()
		{
			return ("Level Data for Debug _ Difficulty: [" + this.levelDifficulty.ToString() + "] " + "Total Waves: [" + this.totalWaves + "] " + "Last Saved Wave: [" + this.lastWaveNumber + "] " + "Final Score: [" + this.totalScore + "] ");
		}

	}

}