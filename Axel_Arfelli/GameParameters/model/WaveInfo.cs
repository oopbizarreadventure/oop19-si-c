﻿using System.Collections.Generic;

namespace model
{
	/// <summary>
	/// Interface for the creation of game-play waves,
	/// where the player and enemies are actors.
	/// </summary>
	public interface WaveInfo
	{

		/// <summary>
		/// Returns an elaborate HP value based on parameters.
		/// </summary>
		/// <param name="baseValue"> </param>
		/// <returns> Enemy HP </returns>
		int elaborateInitialHP(int baseValue);

		/// <summary>
		/// Returns an elaborate DAMAGE value based on parameters.
		/// </summary>
		/// <param name="baseValue"> </param>
		/// <returns> Enemy DAMAGE </returns>
		int elaborateInitialDMG(int baseValue);

		/// <summary>
		/// Returns an elaborate Score Points Amount based on parameters.
		/// </summary>
		/// <param name="enemyTier"> </param>
		/// <returns> Score </returns>
		int elaborateScore(int enemyTier);

		/// <summary>
		/// Returns the difficulty of the WaveInfo.
		/// </summary>
		/// <returns> Difficulty </returns>
		WaveInfo_Difficulty Difficulty {get;}

		/// <summary>
		/// Returns the number of this Wave.
		/// </summary>
		/// <returns> WaveInfo number </returns>
		int Number {get;}

		/// <summary>
		/// Every WaveInfo will have a specific difficulty,
		/// with some specific starting parameters.
		/// </summary>
		/// <returns> Enemy Level, Enemy Damage Multiplier, Enemy Saturation, Enemy Spawn-cap </returns>

	}

	public sealed class WaveInfo_Difficulty
	{
		public static readonly WaveInfo_Difficulty EASY = new WaveInfo_Difficulty("EASY", InnerEnum.EASY, 1, 1, 10);
		public static readonly WaveInfo_Difficulty NORMAL = new WaveInfo_Difficulty("NORMAL", InnerEnum.NORMAL, 5, 2, 15);
		public static readonly WaveInfo_Difficulty HARD = new WaveInfo_Difficulty("HARD", InnerEnum.HARD, 10, 3, 20);

		private static readonly List<WaveInfo_Difficulty> valueList = new List<WaveInfo_Difficulty>();

		static WaveInfo_Difficulty()
		{
			valueList.Add(EASY);
			valueList.Add(NORMAL);
			valueList.Add(HARD);
		}

		public enum InnerEnum
		{
			EASY,
			NORMAL,
			HARD
		}

		public readonly InnerEnum innerEnumValue;
		private readonly string nameValue;
		private readonly int ordinalValue;
		private static int nextOrdinal = 0;

		internal int enemyLEVEL;
		internal int enemyMULTI;
		internal int enemySATUR;

		internal WaveInfo_Difficulty(string name, InnerEnum innerEnum, int enemyLEVEL, int enemyMULTI, int enemySPWNcap)
		{
			this.enemyLEVEL = enemyLEVEL;
			this.enemyMULTI = enemyMULTI;
			this.enemySATUR = enemySPWNcap;

			nameValue = name;
			ordinalValue = nextOrdinal++;
			innerEnumValue = innerEnum;
		}

		/// <summary>
		/// Returns the suggested initial level of enemies.
		/// </summary>
		/// <returns> Suggested initial level </returns>
		public int EnemyInitialLEVEL
		{
			get
			{
				return enemyLEVEL;
			}
		}
		/// <summary>
		/// Returns the difficulty-related multi-purpose Value Multiplier.
		/// It is used for damage, health and spawn cap elaborations.
		/// </summary>
		/// <returns> Multiplier </returns>
		public int EnemyMULTIPLIER
		{
			get
			{
				return enemyMULTI;
			}
		}
		/// <summary>
		/// Returns the suggested limit for screen saturation,
		/// also known as "how many enemies should be on screen at a time".
		/// </summary>
		/// <returns> Enemy Screen Saturation </returns>
		public int EnemySaturationAmount
		{
			get
			{
				return enemySATUR;
			}
		}
		/// <summary>
		/// Returns the suggested limit of enemies per Wave.
		/// </summary>
		/// <returns> Enemy Wave Spawn Cap </returns>
		public int EnemySPAWNcap
		{
			get
			{
				return enemySATUR * enemyMULTI;
			}
		}

		public static WaveInfo_Difficulty[] values()
		{
			return valueList.ToArray();
		}

		public int ordinal()
		{
			return ordinalValue;
		}

		public override string ToString()
		{
			return nameValue;
		}

		public static WaveInfo_Difficulty valueOf(string name)
		{
			foreach (WaveInfo_Difficulty enumInstance in WaveInfo_Difficulty.valueList)
			{
				if (enumInstance.nameValue == name)
				{
					return enumInstance;
				}
			}
			throw new System.ArgumentException(name);
		}
	}

}