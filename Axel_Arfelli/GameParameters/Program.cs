﻿using model;

namespace GameParameters
{
    class Program
    {
        static void Main(string[] args)
        {
            TestParametizer tp = new TestParametizer();
            tp.TestLevelEasy();
            tp.testData();
            tp.testEndless();
        }
    }
}
