﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OOP19_SI.entity;
using OOP19_SI.gameengine;
using OOP19_SI.virtualworld;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace OOP19_SI.gameengine.Tests {
    [TestClass]
    public class GameEnginePrototypeTests {
        readonly IGameEngine Engine = new GameEnginePrototype();

        //There is a bug with running out of debug the it doesn't stop the test
        [TestMethod]
        public void GameEngineProtoypeTest() {
            IStage Stage = new TestStage();
            Engine.Start(Stage);
            Assert.IsTrue(Engine.IsStarted());
            Assert.IsFalse(Stage.IsEnded());
            Engine.Stop();
            Assert.IsFalse(Engine.IsStarted());
            Assert.IsTrue(Stage.IsEnded());
            Console.Out.WriteLine("Score: {0}",Engine.Stage.GetScore());
            Thread.Sleep(500);
        }

        private readonly EntitiesUpdater Updater = new EntitiesUpdater(new GameEnginePrototype.GameLogger(), 20);
        private IEntitySpawner Spawner;
        [TestMethod]
        public void EntitiesUpdaterTest() {
            Thread t = new Thread(Updater.Run);
            Spawner = Updater.GetSpawner();
            t.Start();
            while (!Updater.IsStarted());
            Assert.IsFalse(Updater.IsPaused());
            IActor a = new TestActor(Faction.NEUTRAL, 10);
            Spawner.SpwanActor(a);
            Assert.IsTrue(a.Alive);
            a.AddToLife(-10);
            Assert.IsFalse(a.Alive);
            while (Spawner.GetDespawnedActors().Count == 0);
            Assert.IsTrue(Spawner.GetDespawnedActors().Count == 0);
            Updater.Stop();
            t.Join();
        }

        private class TestActor : IActor {
            public IVirtualBody Body => throw new NotImplementedException();

            public Faction Faction { get; private set; }
            public int Life { get; private set; }
            public IVirtualMap Map { set => throw new NotImplementedException(); }

            public int ScoreValue => 0;

            public bool Alive { get; private set; }

            public string Type => "TestPuppet";

            public string ID => throw new NotImplementedException();

            internal TestActor(Faction Faction, int Life) {
                this.Faction = Faction;
                this.Life = Life;
                Alive = true;
            }

            public void AddToLife(int amount) {
                Life += amount;
                if (Life <= 0) {
                    Alive = false;
                }
            }

            public void Update() {
                Console.Out.WriteLine("I'm Alive! {0}", ToString());
            }

            override
            public String ToString() {
                return String.Format("Type:{0}, HP:{1} ", Type, Life);
            }
        }

        private class TestStage : IStage {

            private bool Alive = false;
            private bool Paused = false;
            private bool Ready = false;
            private int Score = 0;
            private Object lockable = new Object();

            public IEntitySpawner Spawner;

            public int GetScore() {
                return Score;
            }

            public bool IsEnded() {
                return !Alive;
            }

            public bool IsPaused() {
                return Alive && Paused;
            }

            public bool IsReady() {
                return Alive && Ready;
            }

            public void Pause() {
                if (!IsEnded() && !IsPaused()) {
                    Paused = true;
                }
            }

            public void Resume() {
                if (!IsEnded() && IsPaused()) {
                    Paused = false;
                    Monitor.PulseAll(this);
                }
            }

            public void Run() {
                Alive = true;
                Ready = true;
                while (Alive) {
                    if (Paused) {
                        Thread.Sleep(Timeout.Infinite);
                    }
                    Score++;
                }
                Stop();
            }

            public void SetLock(object lockable) {
                this.lockable = lockable;
            }

            public void SetSpawner(IEntitySpawner spawner) {
                this.Spawner = spawner;
            }

            public void Start() {
                throw new NotImplementedException();
            }

            public void Stop() {
                Alive = false;
                Ready = false;
            }
        }
    }
}