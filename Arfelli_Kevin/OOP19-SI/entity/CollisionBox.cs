﻿namespace OOP19_SI.entity {
    public interface ICollisionBox<T> {
        T X { get; }

        T Y { get; }

        T Width { get; }

        T Height { get; }

        bool CheckCollision(ICollisionBox<T> box);
    }
}