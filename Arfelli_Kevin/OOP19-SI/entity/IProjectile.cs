﻿namespace OOP19_SI.entity {

    public interface IProjectile : IEntity {
        /**
    * @return the direction in of the Projectile
    */
        double Direction { get;  }

        /**
         * Notify the Projectile that as hit.
         * need to update the aliveness
         */
        void Hit();

        /**
         * @return the amount of damage
         */
        int Damage { get; }

        /**
         * @return the Actor that generated him.
         */
        IActor Father { get;  }
    }
}
