﻿using OOP19_SI.virtualworld;

namespace OOP19_SI.entity {
    public interface IActor : IEntity {
        int Life { get; }

        void AddToLife(int amount);

        Faction Faction { get;  }

        IVirtualMap Map { set; }
        int ScoreValue { get; }
    }
}