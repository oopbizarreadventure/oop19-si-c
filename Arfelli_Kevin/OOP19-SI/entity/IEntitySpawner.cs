﻿using System.Collections.Generic;

namespace OOP19_SI.entity {
    public interface IEntitySpawner {
        void SpwanProjectile(IProjectile p);

        void SpwanActor(IActor a);

        ISet<IActor> GetDespawnedActors();

        ISet<IProjectile> GetDespawnedProjectiles();
    }
}