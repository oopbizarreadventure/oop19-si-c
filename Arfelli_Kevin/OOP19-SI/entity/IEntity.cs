﻿using OOP19_SI.virtualworld;
using System;
using System.Collections.Generic;
using System.Text;

namespace OOP19_SI.entity
{
    public interface IEntity {

        void Update();

        /**
         * @return returns a bool whenever the entity is alive.
         */
        bool Alive { get; }

        /**
         * @return the type of entity.
         */
        String Type { get; }


        /**
         * @return the VirtualBody of the entity
         */
        IVirtualBody Body { get; }

        /**
         * @return The Unique identifier of the entity
         */
        String ID { get; }
    }

}
