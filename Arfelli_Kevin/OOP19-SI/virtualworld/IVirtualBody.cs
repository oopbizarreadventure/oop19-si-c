﻿using OOP19_SI.entity;
using System;
using System.Collections.Generic;
using System.Text;

namespace OOP19_SI.virtualworld {
    public interface IVirtualBody {
        /**
         * Move the virtualBody if possible and if the movementcontroll is present.
         * @param xvalue Is the X coordinate
         * @param yvalue Is the Y coordinate
         * @return return a bool representing the action appening
         */
        bool Move(int xvalue, int yvalue);

        /**
         * @return The collisionBox
         */
        ICollisionBox<int> CollisionBox { get; }

        /**
         * Check the collision between two virtualBody.
         * @param box
         * @return A bool representing the state
         */
        bool CheckCollision(ICollisionBox<int> box);

        /**
         * Set the actual movement in reference to a map.
         * @param movementcontrol
         */
        void SetMotion(IMovment movementcontrol);

        /** 
         * @return an optional VirtualMap
         */
        IVirtualMap Map { get; }
    }


}

