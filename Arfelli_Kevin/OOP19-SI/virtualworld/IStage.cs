﻿using OOP19_SI.entity;
using System;
using System.Collections.Generic;
using System.Text;

namespace OOP19_SI.virtualworld {
    public interface IStage {
        void Start();
        void Run();
        void Stop();
        bool IsEnded();

        void Pause();
        void Resume();
        bool IsPaused();
        bool IsReady();

        int GetScore();

        void SetSpawner(IEntitySpawner spawner);
        void SetLock(Object lockable);
    }
}
