﻿using OOP19_SI.entity;
using OOP19_SI.virtualworld;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using static OOP19_SI.gameengine.IGameLogger;

namespace OOP19_SI.gameengine {
    public class EntitiesUpdater {
        private readonly IList<IProjectile> Projectiles = new List<IProjectile>();
        private readonly IList<IActor> Actors = new List<IActor>();
        private IGameLogger logger;
        private int tick;
        private object lockable;
        Stopwatch stopwatch = new Stopwatch();

        public bool Alive { get; private set; }
        public bool Paused { get; private set; }
        public object Long { get; private set; }


        public EntitiesUpdater(IGameLogger logger, int tick) : this(logger,tick,new Object()) { }
        public EntitiesUpdater(IGameLogger logger, int tick, object lockable) {
            this.logger = logger;
            this.tick = tick;
            this.lockable = lockable;
        }

        public IEntitySpawner GetSpawner() {
            return new EntitySpawner(this);
        }

        private ISet<IProjectile> CleanDeadProjectiles() {
            var result = Projectiles
                .Where(p => !p.Alive)
                .Select(p => p);

            var set = new HashSet<IProjectile>(result);
            foreach (var e in set) {
                Projectiles.Remove(e);
            }
            return set;
        }

        private ISet<IActor> CleanDeadActors() {
            var result = Actors
                .Where(p => !p.Alive)
                .Select(p => p);

            var set = new HashSet<IActor>(result);
            foreach (var e in set) {
                Actors.Remove(e);
            }
            return set;
        }

        public class EntitySpawner : IEntitySpawner {
            private readonly EntitiesUpdater Outter;

            public EntitySpawner(EntitiesUpdater outter) {
                this.Outter = outter; 
            }
            void IEntitySpawner.SpwanProjectile(IProjectile p) {
                lock (Outter.Projectiles) {
                    Outter.Projectiles.Add(p);
                }
            }

            void IEntitySpawner.SpwanActor(IActor a) {
                lock (Outter.Actors) {
                    Outter.Actors.Add(a);
                }
            }

            ISet<IActor> IEntitySpawner.GetDespawnedActors() {
                lock (Outter.Actors) {
                    ISet<IActor> set = Outter.CleanDeadActors();
                    Outter.logger.logLine(String.Format("Getting {0} Dead Actors", set.Count), IGameLogger.OutputLevel.DEBUG);
                    return set;
                }
            }

            ISet<IProjectile> IEntitySpawner.GetDespawnedProjectiles() {
                lock (Outter.Projectiles) {
                    ISet<IProjectile> set = Outter.CleanDeadProjectiles();
                    Outter.logger.logLine(String.Format("Getting {0} Dead Projectiles", set.Count), IGameLogger.OutputLevel.DEBUG);
                    return set;
                }
            }

        }

        public void Run() {
            Alive = true;
            while (Alive) {
                if (Paused) {
                    try {
                        Thread.Sleep(Timeout.InfiniteTimeSpan);
                    }
                    catch (ThreadInterruptedException e) {
                        if (IsPaused()) {
                            logger.logLine("Thread-Entities have been wakened!", IGameLogger.OutputLevel.DEBUG);
                        }
                        else {
                            logger.logLine("Thread-Entities have been wakened too early!", IGameLogger.OutputLevel.DEBUG);
                        }
                    }
                }
                else {

                    stopwatch.Start();
                    logger.logLine("Start Updating Entities!", IGameLogger.OutputLevel.DEBUG);
                    long updatedprojectiles = UpdateProjetiles();
                    CheckCollisions();
                    long updatedactors = UpdateActors();
                    logger.logLine(String.Format("Entities Updated: {0}A {1}P", updatedactors, updatedprojectiles), OutputLevel.DEBUG);
                    stopwatch.Stop();
                    while (stopwatch.ElapsedMilliseconds < (1000 / tick)) {
                        try {
                            Thread.Sleep((1000 / tick) - Convert.ToInt32(stopwatch.ElapsedMilliseconds));
                        }
                        catch (ThreadInterruptedException e) {
                        }
                        stopwatch.Restart();
                    }
                    logger.logLine(String.Format("Cicle time was {0}!", stopwatch.ElapsedMilliseconds), OutputLevel.DEBUG);
                    stopwatch.Reset();
                }
            }
            Stop();
        }

        private long UpdateActors() {
            ISet<IActor> set;
            lock(Actors) {
                set = new HashSet<IActor>(Actors);
            }
            set = set
                .AsParallel()
                .Where(a => a.Alive)
                .ToHashSet();
            foreach (var a in set) {
                a.Update();
            }
            return set.Count;
        }

        private void CheckCollisions() {
            lock(lockable) {
                this.Projectiles
                    .AsParallel()
                    .Where(x => x.Alive)
                    .ForAll(x => {
                        IActor dead = this.Actors
                        .Where(y => y.Alive)
                        .Where(y => x.Father.Faction != y.Faction)
                        .Where(y => x.Body.CheckCollision(y.Body.CollisionBox))
                        .FirstOrDefault(null);
                        if (dead != null) {
                            dead.AddToLife(-x.Damage);
                            x.Hit();
                        }
                    });
            }
        }

        private long UpdateProjetiles() {
            ISet<IProjectile> set;
            lock (Actors) {
                set = new HashSet<IProjectile>(Projectiles);
            }
            set = set
                .AsParallel()
                .Where(a => a.Alive)
                .ToHashSet();
            foreach (var a in set) {
                a.Update();
            }
            return set.Count;
        }

        public bool IsStarted() {
            return this.Alive;
        }
        public bool IsPaused() {
            return this.Alive && this.Paused;
        }

        public void Stop() {
            if (this.Alive) {
                this.Alive = false;
                this.CleanAll();
            }
        }

        private void CleanAll() {
            lock(this) {
                Actors.Clear();
                Projectiles.Clear();
            }
        }

        public void Pause() {
            this.Paused = true;
        }

        public void Resume() {
            lock (this) {
                if (this.IsPaused()) {
                    this.Paused = true;
                    Monitor.PulseAll(this);
                }            
            }
        }


    }
}