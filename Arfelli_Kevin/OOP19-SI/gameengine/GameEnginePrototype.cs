﻿using OOP19_SI.virtualworld;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using static OOP19_SI.gameengine.IGameLogger;

namespace OOP19_SI.gameengine {
    public class GameEnginePrototype : IGameEngine {
        private bool alive = false;

        public IStage Stage { get; private set; }

        private bool paused = false;
        private const int STANDARDTICK = 20;
        private readonly int tick;
        private EntitiesUpdater entities;
        private ISet<Thread> threads;

        private IGameLogger logger;
        private readonly Object lockable = new Object();

        public GameEnginePrototype() : this(STANDARDTICK) { }

        public GameEnginePrototype(int tick) : this(tick, new GameLogger()) { }

        /**
         * @param tick
         * @param logger
         */
        GameEnginePrototype(int tick, GameLogger logger) {
            this.tick = tick;
            this.logger = logger;
        }

        public bool IsPaused() {
            return paused && alive;
        }

        public bool IsStarted() {
            return alive;
        }

        public void Pause() {
            lock(lockable) {
                if (!IsPaused() && IsStarted()) {
                    this.paused = true;
                    this.Stage.Pause();
                    this.entities.Pause();
                    logger.logLine("GameEngine paused", OutputLevel.DEBUG);
                }
            }
        }

        public void Resume() {
            lock(lockable) {
                if (IsPaused()) {
                    this.paused = false;
                    this.Stage.Resume();
                    this.entities.Resume();
                    logger.logLine("GameEngine resume", OutputLevel.DEBUG);
                }
            }
        }

        public void Start(IStage stage) {
            lock(lockable) {
                if (!this.IsStarted()) {
                    this.alive = true;
                    this.Stage = stage;
                    this.Stage.SetLock(lockable) ;
                    this.entities = new EntitiesUpdater(logger, tick, this.lockable) ;
                    this.Stage.SetSpawner(this.entities.GetSpawner());
                    threads = new HashSet<Thread>(new List<Thread> { new Thread(start: this.entities.Run), new Thread(this.Stage.Run) });
                    foreach (var x in threads){
                        x.Start();
                    }
                    logger.logLine("GameEngine Started!", OutputLevel.LOG);
                }
            }

        }

        public void Stop() {
            if (IsStarted()) {
                this.alive = false;
                this.Resume();
                this.Stage.Stop();
                this.entities.Stop();
                this.alive = false;
                foreach (Thread t in threads) {
                    t.Join();
                }
                logger.logLine("GameEngine Stopped!", OutputLevel.LOG);
            }
            
        }

        public class GameLogger : IGameLogger {
            private string Format(OutputLevel level, string line) {
                return string.Format("|{0}|:{1}", level, line);
            }

            public void logLine(string line, OutputLevel level) {
                Console.WriteLine(string.Format("{0:dd/MM/yyyy HH:mm:ss} {1}", new DateTime(), Format(level, line)));
            }
        }
    }
}
