﻿using OOP19_SI.virtualworld;
using System;
using System.Collections.Generic;
using System.Text;

namespace OOP19_SI.gameengine {
    public interface IGameEngine {
        /**
         * Starts the Game Engine.
         * Could throw IllegalThreadStateException if already started.
         * @param stage set the running stage
         * @throw IllegalThreadStateException
         * */
        void Start(IStage stage);

        /**
         * Stops the Game Engine.
         * */
        void Stop();

        /**
         * @return return if the engine is been started
         * */
        bool IsStarted();

        /**
         * @return if the engine is been paused
         */
        bool IsPaused();

        /**
         * Pause the engine.
         */
        void Pause();

        /**
         * Resume the engine.
         */
        void Resume();

        /**
         * Return the last stage.
         * @return the Nullable of the last stage
         */
        IStage Stage { get; }
    }
}
