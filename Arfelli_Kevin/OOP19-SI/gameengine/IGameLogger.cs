﻿namespace OOP19_SI.gameengine {
    public interface IGameLogger {
        enum OutputLevel { LOG, ERROR, DEBUG }
        void logLine(string line, OutputLevel level);
    }
}