﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Barbaro.entity;

namespace EntityTest
{

    [TestClass]
    public class EntityTest
    {
        public IActor drone = new Drone(500, 600, 1, 1);
        public IActor muncher = new Muncher(300, 700, 1, 1);
        public IActor turret = new Turret(100, 200, 1, 1);
        public IActor boss = new EnemyBoss(900, 100, 1, 1);

        [TestMethod]
        public void TestDrone()
        {
            Assert.AreEqual(drone.Body.Hitbox.X, 500);
            Assert.AreEqual(drone.Body.Hitbox.Y, 600);
            Assert.AreEqual(drone.IsAlive(), true);
        }

        [TestMethod]
        public void TestMuncher()
        {
            Assert.AreEqual(muncher.Body.Hitbox.X, 300);
            Assert.AreEqual(muncher.Body.Hitbox.Y, 700);
            Assert.AreEqual(muncher.IsAlive(), true);
        }

        [TestMethod]
        public void TestTurret()
        {
            Assert.AreEqual(turret.Body.Hitbox.X, 100);
            Assert.AreEqual(turret.Body.Hitbox.Y, 200);
            Assert.AreEqual(turret.IsAlive(), true);
        }

        [TestMethod]
        public void TestBoss()
        {
            Assert.AreEqual(boss.Body.Hitbox.X, 900);
            Assert.AreEqual(boss.Body.Hitbox.Y, 100);
            Assert.AreEqual(boss.IsAlive(), true);
        }

        [TestMethod]
        public void TestUpdate()
        {
            drone.Update();
            Assert.AreEqual(drone.Body.Hitbox.X, 501);
            boss.Update();
            Assert.AreEqual(boss.Body.Hitbox.X, 901);
        }

        [TestMethod]
        public void TestDeath()
        {
            drone.AddToLife(-1);
            muncher.AddToLife(-1);
            turret.AddToLife(-1);
            boss.AddToLife(-1);
            Assert.AreEqual(drone.IsAlive(), false);
            Assert.AreEqual(muncher.IsAlive(), false);
            Assert.AreEqual(turret.IsAlive(), false);
            Assert.AreEqual(boss.IsAlive(), false);
        }
    }
}
