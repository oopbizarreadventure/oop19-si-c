﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Barbaro.entity;

namespace OOP19_SI.virtualworld
{
    public interface IMovment
    {
        bool MoveTo(int x, int y);
        IVirtualMap GetMap();
        CollisionBoxInt GetCollisionBox();
    }
}