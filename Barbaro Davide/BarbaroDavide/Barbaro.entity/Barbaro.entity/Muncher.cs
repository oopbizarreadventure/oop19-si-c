﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Barbaro.entity
{
    public class Muncher : SimpleEnemy
    {

        public static int BASE_LIFE = 15;
        private static int MUNCHER_BULLET_WIDTH = 8;
        private static int MUNCHER_BULLET_HEIGHT = 8;
        private static int CHARGE_RANGE = 30;
        private static int CHARGE_TRESHOLD_START = 30;
        private static int MUNCHER_SCORE = 15;
        private static int CHARGE_COOLDOWN = 20;
        private int chargeTreshold;
        private int initialHeigth;
        private int chargeCount;
        private bool charge = false;
        private Random randomCharge = new Random();
        private bool down;

        public Muncher(int x, int y, int enemyLvl, int life) : base(x, y, enemyLvl)
        {
            this.charge = false;
            this.chargeTreshold = CHARGE_TRESHOLD_START - enemyLvl;
            Life = life;
            this.initialHeigth = y;
            Type = "Muncher";
            Score = MUNCHER_SCORE + level;
        }

        public Muncher(KeyValuePair<int, int> pos, int enemyLvl, int life) : base(pos.Key, pos.Value, enemyLvl)
        {
            this.charge = false;
            this.chargeTreshold = CHARGE_TRESHOLD_START - enemyLvl;
            Life = life;
            this.initialHeigth = pos.Value;
            Type = "Muncher";
            Score = MUNCHER_SCORE + level;
        }
        override public void Update()
        {
            Move();
            TryAndShoot();
        }
        override protected void Move()
        {
            if (!charge)
            {
                IActor optPlayer = this.Body.Map.GetActors(Faction.ALLY).Keys.ElementAt(0);
                if (optPlayer != null)
                {
                    if (this.Body.Hitbox.X <= optPlayer.Body.Hitbox.X)
                    {
                        this.Body.Move(Body.Hitbox.X + 1, Body.Hitbox.Y);
                    }
                    else
                    {
                        this.Body.Move(Body.Hitbox.X - 1, Body.Hitbox.Y);
                    }
                    if (this.randomCharge.Next(CHARGE_RANGE) >= chargeTreshold)
                    {
                        if (chargeCount == CHARGE_COOLDOWN)
                        {
                            chargeCount = 0;
                            this.charge = true;
                            this.down = true;
                        }
                        else
                        {
                            chargeCount++;
                        }
                    }
                }
            }
            else
            {
                if (this.down)
                {
                    this.Body.Move(Body.Hitbox.X, Body.Hitbox.Y - 3);
                    if (this.Body.Map == null)
                    {
                        return;
                    }
                    else if (Body.Hitbox.Y - this.length <= 0)
                    {
                        this.down = false;
                    }
                }
                else
                {
                    this.Body.Move(Body.Hitbox.X, Body.Hitbox.Y + 2);
                    if (Body.Hitbox.Y >= this.initialHeigth)
                    {
                        this.charge = false;
                    }
                }
            }

        }
        override protected int GetAngle()
        {
            return ANGLE_DOWN;
        }

        override protected int GetBulletWidth()
        {
            return MUNCHER_BULLET_WIDTH;
        }

        override protected int GetBulletHeight()
        {
            return MUNCHER_BULLET_HEIGHT;
        }
    }
}
