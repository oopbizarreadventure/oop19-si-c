﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OOP19_SI.virtualworld;

namespace Barbaro.entity
{
    public abstract class SimpleShip : IActor
    {
        public int Life { get; set; }
        protected bool isAlive = true;
        public IVirtualBody Body { get; set; }
        protected int length = 64;
        public int Score { get; set; }
        public string Type { get; set; }

        public SimpleShip(int x, int y)
        {
            this.Body = new BodyImpl(new CollisionBoxInt(x, y, length, length));
        }


        public abstract void Update();


        public bool IsAlive()
        {
            return this.isAlive;
        }

        public void AddToLife(int amount)
        {
            Life = Life + amount;
            if (Life <= 0)
            {
                this.isAlive = false;
            }
        }

        public abstract Faction GetFaction();

        protected abstract int GetBulletWidth();

        protected abstract int GetBulletHeight();

        virtual protected void Shoot(int shootingAngle)
        {
            if (this.Body.Map == null)
            {
                return;
            }

            try
            {
                this.Body.Map.AddProjectile(new GenericProjectile(new CollisionBoxInt(Body.Hitbox.X + (this.length / 2), Body.Hitbox.Y + (this.length / 2), GetBulletWidth(),
                        GetBulletHeight()), shootingAngle, this), Body.Hitbox.X + (this.length / 2), Body.Hitbox.Y + (this.length / 2));
            }
            catch
            {
            }

        }

        public void SetMap(IVirtualMap map)
        {
            //((SimpleShip) this.body).setMap(map);
            ((BodyImpl)Body).Map = map;
        }

    }
}
