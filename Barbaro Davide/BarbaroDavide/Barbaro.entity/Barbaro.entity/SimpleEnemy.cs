﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Barbaro.entity
{
    public abstract class SimpleEnemy : SimpleShip
    {

        private static int MAX_RANGE = 20;
        private static int RANGE_MULTIPL = 80;
        protected static int ANGLE_DOWN = 270;
        private Random rand = new Random();
        private int count;
        protected int level;

        public SimpleEnemy(int x, int y, int enemyLvl) : base(x, y)
        {
            this.level = enemyLvl;
        }

        override public void Update()
        {
            Move();
            TryAndShoot();
        }

        protected abstract void Move();

        override public Faction GetFaction()
        {
            return Faction.ENEMY;
        }

        protected int PlayerAngle()
        {
            if (this.Body.Map == null)
            {
                return ANGLE_DOWN;
            }
            else
            {
                IActor optPlayer = this.Body.Map.GetActors(Faction.ALLY).Keys.ElementAt(0);
                if (optPlayer != null)
                {
                    double playerX = optPlayer.Body.Hitbox.X;
                    double playerY = optPlayer.Body.Hitbox.Y;
                    double ipotenusa = Math.Sqrt(Math.Pow(playerX - this.Body.Hitbox.X, 2) + Math.Pow(playerY - this.Body.Hitbox.Y, 2));
                    double angle = Math.Asin(Math.Abs(playerX - this.Body.Hitbox.X) / ipotenusa);
                    if (this.Body.Hitbox.X >= playerX && this.Body.Hitbox.Y >= playerY)
                    {
                        return 270 - (int)angle;
                    }
                    else if (this.Body.Hitbox.Y >= playerY)
                    {
                        return 270 + (int)angle;
                    }
                    else if (this.Body.Hitbox.X >= playerX && this.Body.Hitbox.Y < playerY)
                    {
                        return (int)angle;
                    }
                    else
                    {
                        return (int)angle;
                    }
                }
                else
                {
                    return ANGLE_DOWN;
                }
            }
        }

        protected abstract int GetAngle();
        
        virtual protected void TryAndShoot()
        {

            int testValue = rand.Next(MAX_RANGE);
            if (testValue >= RANGE_MULTIPL - this.level - this.count)
            {
                this.count = 0;
                Shoot(GetAngle());
            }
            else
            {
                this.count++;
            }
        }
    }
}
