﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OOP19_SI.virtualworld;

namespace Barbaro.entity
{
    public interface IEntity
    {
         /**
         *  Update the status of the entity.
         *  If is an enemy it also moves and tries to shoot.
         */
        void Update();

        /**
         * @return returns a boolean whenever the entity is alive.
         */
        bool IsAlive();

        /**
         * @return the type of entity.
         */
        String Type { get; }


        /**
         * @return the VirtualBody of the entity
         */
        IVirtualBody Body { get; }
    }
}
