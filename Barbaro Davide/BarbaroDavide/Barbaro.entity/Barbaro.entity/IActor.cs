﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OOP19_SI.virtualworld;

namespace Barbaro.entity
{
    public interface IActor : IEntity
    {
         /**
         * @return the amount of life of the Actor.
         */
        int Life { get; }

        /**
         * Add the amount to the Actor.
         * @param amount of life (even negative)
         */
        void AddToLife(int amount);

        /**
         * Define the Faction of the Actor.
         * @return the Faction of affiliation
         */
        Faction GetFaction();

        /**
         * Set the VirtualMap.
         * @param map
         */
        void SetMap(IVirtualMap map);

        /**
         * @return the Score value of the Actor.
         */
        int Score { get; }
    }
}
