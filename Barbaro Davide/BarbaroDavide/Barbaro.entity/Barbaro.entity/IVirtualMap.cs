﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Barbaro.entity;

namespace OOP19_SI.virtualworld
{
    public interface IVirtualMap
    {
        int Height { get; }
        int Width { get; }
        void AddProjectile(GenericProjectile projectile, int x, int y);
        void AddProjectile(GenericProjectile projectile, CollisionBoxInt collisionbox);
        Dictionary<IActor, CollisionBoxInt> GetActors(Faction faction);
        Dictionary<IActor, CollisionBoxInt> GetActors();
    }
}