﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OOP19_SI.virtualworld;

namespace Barbaro.entity
{
    public class BodyImpl : IVirtualBody
    {
        public CollisionBoxInt Hitbox { get; }
        public IVirtualMap Map { get; set; }
        private IMovment movmentControl = null;

        /**
         * @param hitbox
         */
        public BodyImpl(CollisionBoxInt hitbox)
        {
            this.Hitbox = hitbox;
        }

        public bool Move(int x, int y)
        {
            this.Hitbox.X = x;
            this.Hitbox.Y = y;
            return true;
        }

        public CollisionBoxInt GetCollisionBox()
        {
            return Hitbox;
        }

        public void SetMotion(IMovment movmentcontrol)
        {
            this.movmentControl = movmentcontrol;
        }

        public bool CheckCollision(CollisionBoxInt box)
        {
            return Hitbox.CheckCollision(box);
        }

        public IVirtualMap GetMap()
        {
            if (movmentControl == null)
            {
                return null;
            }

            return movmentControl.GetMap();
        }
        
    }
}
