﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OOP19_SI.virtualworld;

namespace Barbaro.entity
{
    public class PlayerShip : SimpleShip
    {
        private static int PRIMARY_BULLET_WIDTH = 8;
        private static int PRIMARY_BULLET_HEIGHT = 8;
        private static int UP_ANGLE = 90;
        private int tempx = 0;
        private int tempy = 0;

        public PlayerShip(int x, int y) : base(x, y)
        {
            Life = 15000;
            Type = "Player";
        }

        override public void Update()
        {
            this.Body.Move(tempx, tempy);
        }

        override public Faction GetFaction()
        {
            return Faction.ALLY;
        }

        override protected int GetBulletWidth()
        {
            return PRIMARY_BULLET_WIDTH;
        }

        override protected int GetBulletHeight()
        {
            return PRIMARY_BULLET_HEIGHT;
        }

        public void fire()
        {
            Shoot(UP_ANGLE);
        }

        public new void SetMap(IVirtualMap map)
        {
            return;
        }

        public void move(int x, int y)
        {
            this.tempx = x;
            this.tempy = y;
        }
    }
}
