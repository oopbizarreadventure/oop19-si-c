﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Barbaro.entity
{
    public class CollisionBoxInt
    {
        public int X { get; set; }
        public int Y { get; set; }
        public int Width { get; }
        public int Height { get; }


        public CollisionBoxInt(int xvalue, int yvalue, int width, int height)
        {
            X = xvalue;
            Y = yvalue;
            Width = width;
            Height = height;
        }

        public bool CheckCollision(CollisionBoxInt box)
        {
            return false;
        }

    }
}
