﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Barbaro.entity
{
    public class Drone : SimpleEnemy
    {
        public static int BASE_LIFE = 8;
        private static int DRONE_BULLET_WIDTH = 8;
        private static int DRONE_BULLET_HEIGHT = 8;
        private static int RIGHT = 1;
        private static int LEFT = 0;
        private static int DRONE_SCORE = 10;
        private int direction = RIGHT;

        public Drone(int x, int y, int enemyLvl, int life) : base (x, y, enemyLvl)
        {
            Life = life;
            Type = "Drone";
            Score = DRONE_SCORE + level;
        }

        public Drone(KeyValuePair<int, int> pos, int enemyLvl, int life) : base(pos.Key, pos.Value, enemyLvl)
        {
            Life = life;
            Type = "Drone";
            Score = DRONE_SCORE + level;
        }

        override public void Update()
        {
            Move();
            TryAndShoot();
        }

        override protected void Move()
        {
            if (direction == RIGHT)
            {
                this.Body.Move(this.Body.Hitbox.X + 1, this.Body.Hitbox.Y);
                if (this.Body.Map == null)
                {
                    return;
                }
                else if (this.Body.Hitbox.X + this.length >= this.Body.Map.Width - this.length / 2)
                {
                    this.direction = LEFT;
                }
            }
            else
            {
                this.Body.Move(this.Body.Hitbox.X - 1, this.Body.Hitbox.Y);
                if (this.Body.Hitbox.X - this.length <= this.length / 2)
                {
                    this.direction = RIGHT;
                }
            }
        }

        override protected int GetBulletWidth()
        {
            return DRONE_BULLET_WIDTH;
        }

        override protected int GetBulletHeight()
        {
            return DRONE_BULLET_HEIGHT;
        }

        override protected int GetAngle()
        {
            return PlayerAngle();
        }
    }
}
