﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Barbaro.entity
{
    public class Faction
    {

        /// <summary>
        /// Represents the ENEMY faction.
        /// </summary>
        public static readonly Faction ENEMY = new Faction("ENEMY", InnerEnum.ENEMY);
        /// <summary>
        /// Represents NEUTRAL faction. 
        /// </summary>
        public static readonly Faction NEUTRAL = new Faction("NEUTRAL", InnerEnum.NEUTRAL);
        /// <summary>
        /// Represents Player Faction.
        /// </summary>
        public static readonly Faction ALLY = new Faction("ALLY", InnerEnum.ALLY);

        private static readonly List<Faction> valueList = new List<Faction>();

        static Faction()
        {
            valueList.Add(ENEMY);
            valueList.Add(NEUTRAL);
            valueList.Add(ALLY);
        }

        public enum InnerEnum
        {
            ENEMY,
            NEUTRAL,
            ALLY
        }

        public readonly InnerEnum innerEnumValue;
        private readonly string nameValue;
        private readonly int ordinalValue;
        private static int nextOrdinal = 0;

        private Faction(string name, InnerEnum innerEnum)
        {
            nameValue = name;
            ordinalValue = nextOrdinal++;
            innerEnumValue = innerEnum;
        }

        /// <summary>
        /// Return if a Faction is against another one. </summary>
        /// <param name="faction"> to evaluate </param>
        /// <returns> return boolean representing if against </returns>

        public bool IsOpposed(Faction faction)
        {
            if (faction == Faction.NEUTRAL)
            {
                return false;
            }
            return this != faction;
        }

        public static Faction[] values()
        {
            return valueList.ToArray();
        }

        public int ordinal()
        {
            return ordinalValue;
        }

        public override string ToString()
        {
            return nameValue;
        }

        public static Faction valueOf(string name)
        {
            foreach (Faction enumInstance in Faction.valueList)
            {
                if (enumInstance.nameValue == name)
                {
                    return enumInstance;
                }
            }
            throw new System.ArgumentException(name);
        }
    }
}

