﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Barbaro.entity
{
    public class GenericProjectile : IProjectile
    {
        /**
        * It's define the type of Projectile.
        */
        public static String TYPE = "Generic-Projectile";
        /**
         * It's define the Damage of the projectile.
         */
        private static int DAMAGE = 1;
        /**
         * It's define the Velocity of the projectile.
         */
        private static int VELOCITY = 5;

        private bool alive = new bool();
        public double Angle { get; set; }
        public IActor Father { get; set; }
        public IVirtualBody Body { get; set; }
        public int Damage { get; } = 1;
        public string Type { get; } = GenericProjectile.TYPE;

        private double velocity = VELOCITY;

        public GenericProjectile(CollisionBoxInt box, double angle, IActor father)
        {
            Body = new BodyImpl(box);
            this.alive = true;
            Angle = angle;
            Father = father;
        }

        public void Update()
        {
            if (this.alive)
                {
                    KeyValuePair<int, int> nextpoint = getNextPoint(this.Body.Hitbox.X, this.Body.Hitbox.Y);
                    if (!this.Body.Move(nextpoint.Key, nextpoint.Value))
                    {
                        this.Hit();
                    }
                }
        }

        private KeyValuePair<int, int> getNextPoint(int x, int y)
        {
            return new KeyValuePair<int, int>((int)(x + ((Math.Cos((Angle)) * velocity))), (int)(y + (Math.Sin((Angle)) * velocity)));
        }

        public bool IsAlive()
        {
            return this.alive;
        }

        public new String GetType()
        {
            return GenericProjectile.TYPE;
        }

        public void Hit()
        {
            this.alive = false;
        }

    }
}
