﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Barbaro.entity
{
    public class Turret : SimpleEnemy
    {

        public static int BASE_LIFE = 12;
        private static int TURRET_BULLET_WIDTH = 8;
        private static int TURRET_BULLET_HEIGHT = 8;
        private static int TURRET_SCORE = 12;

        public Turret(int x, int y, int enemyLvl, int life) : base(x, y, enemyLvl)
        {
            Life = life;
            Type = "Turret";
            Score = TURRET_SCORE + level;
        }

        public Turret(KeyValuePair<int, int> pos, int enemyLvl, int life) : base(pos.Key, pos.Value, enemyLvl)
        {
            Type = "Turret";
            Life = life;
            Score = TURRET_SCORE + level;
        }

        override public void Update()
        {
            Move();
            TryAndShoot();
        }

        override protected void Move()
        {
            //La Turret resta immobile dove viene inizialmente spawnata
            return;
        }

        override protected int GetBulletWidth()
        {
            return TURRET_BULLET_WIDTH;
        }

        override protected int GetBulletHeight()
        {
            return TURRET_BULLET_HEIGHT;
        }

        override protected int GetAngle()
        {
            return PlayerAngle();
        }

    }
}
