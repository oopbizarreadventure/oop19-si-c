﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Barbaro.entity
{
    public interface IProjectile : IEntity
    {
        double Angle { get; }

        /**
         * Notify the Projectile that as hit.
         * need to update the aliveness
         */
        void Hit();

        /**
         * @return the amount of damage
         */
        int Damage { get; }

        /**
         * @return the Actor that generated him.
         */
        IActor Father { get; }
    }

}
