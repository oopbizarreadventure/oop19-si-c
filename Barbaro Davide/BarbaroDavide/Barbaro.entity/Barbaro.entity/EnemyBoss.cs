﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Barbaro.entity
{
    public class EnemyBoss : SimpleEnemy
    {
        public static int BASE_LIFE = 200;
        private static int RIGHT = 1;
        private static int LEFT = 0;
        private static int BARRAGE_WIDTH = 5;
        private static int BARRAGE_LENGTH = 5;
        private static int BULLETHELL_WIDTH = 8;
        private static int BULLETHELL_LENGTH = 8;
        private static int MITRA_WIDTH = 5;
        private static int MITRA_LENGTH = 5;
        private static int MAX_RANGE = 8;
        private static int RANGE_MULTIPL = 16;
        private static int ATKS_COUNT = 3;
        private static int DIRECTION_DOWN = 270;
        private static int ATK_LENGTH = 12;
        private static int BOSS_SIZE = 128;
        private static int BOSS_SCORE = 1000;
        private int ATK_COOLDOWN = 10;
        private int direction = RIGHT;
        private int atkCount = 0;
        private int atkCooldown = 0;
        private Random rand = new Random();
        private int count = 0;
        private bool shooting = false;
        private Attacks currentAtk = Attacks.barrage;
        protected new int length = BOSS_SIZE;

        public EnemyBoss(int x, int y, int enemyLvl, int life) : base(x, y, enemyLvl)
        {
            Life = life;
            Score = BOSS_SCORE + level;
            Type = "Boss";
        }

        public EnemyBoss(KeyValuePair<int, int> pos, int enemyLvl, int life) : base(pos.Key, pos.Value, enemyLvl)
        {
            Life = life;
            Score = BOSS_SCORE + level;
            Type = "Boss";
        }

        override protected void Move()
        {
            if (direction == RIGHT)
            {
                this.Body.Move(this.Body.Hitbox.X + 1, this.Body.Hitbox.Y);
                if (this.Body.Map == null)
                {
                    return;
                }
                else if (this.Body.Hitbox.X + this.length >= this.Body.Map.Width - this.length / 2)
                {
                    this.direction = LEFT;
                }
            }
            else
            {
                this.Body.Move(this.Body.Hitbox.X - 1, this.Body.Hitbox.Y);
                if (this.Body.Hitbox.X - this.length <= this.length / 2)
                {
                    this.direction = RIGHT;
                }
            }
        }
        override protected void TryAndShoot()
        {
            if (!shooting)
            {
                int testValue = rand.Next(MAX_RANGE);
                if (testValue >= RANGE_MULTIPL - this.level - this.count)
                {
                    this.count = 0;
                    this.shooting = true;
                    int atkType = rand.Next(ATKS_COUNT);
                    switch (atkType)
                    {
                        case 0:
                            this.currentAtk = Attacks.barrage;
                            break;
                        case 1:
                            this.currentAtk = Attacks.bulletHell;
                            break;
                        case 2:
                            this.currentAtk = Attacks.mitra;
                            break;
                    }
                    this.shooting = true;
                }
                else
                {
                    this.count++;
                }
            }
            else
            {
                if (atkCooldown >= ATK_COOLDOWN)
                {
                    atkCooldown = 0;
                    atkCount++;
                    Shoot(GetAngle());
                    if (atkCount >= ATK_LENGTH)
                    {
                        this.shooting = false;
                        atkCount = 0;
                    }
                }
                atkCooldown++;
            }
        }
        override protected void Shoot(int shootingAngle)
        {
            if (this.Body.Map == null)
            {
                return;
            }
            else NewMethod(shootingAngle);
        }

        private void NewMethod(int shootingAngle)
        {
            if (this.currentAtk == Attacks.barrage)
            {
                try
                {
                    this.Body.Map.AddProjectile(new GenericProjectile(new CollisionBoxInt(Body.Hitbox.X + (this.length / 2), Body.Hitbox.Y + (this.length / 2), GetBulletWidth(),
                            GetBulletHeight()), shootingAngle, this), Body.Hitbox.X + (this.length / 2), Body.Hitbox.Y + (this.length / 2));
                }
                catch
                {
                }
            }
            else if (this.currentAtk == Attacks.bulletHell)
            {
                try
                {
                    this.Body.Map.AddProjectile(new GenericProjectile(new CollisionBoxInt(Body.Hitbox.X + (this.length / 2), Body.Hitbox.Y + (this.length / 2), GetBulletWidth(),
                            GetBulletHeight()), shootingAngle + 40, this), Body.Hitbox.X + (this.length / 2), Body.Hitbox.Y + (this.length / 2));
                    this.Body.Map.AddProjectile(new GenericProjectile(new CollisionBoxInt(Body.Hitbox.X + (this.length / 2), Body.Hitbox.Y + (this.length / 2), GetBulletWidth(),
                            GetBulletHeight()), shootingAngle + 20, this), Body.Hitbox.X + (this.length / 2), Body.Hitbox.Y + (this.length / 2));
                    this.Body.Map.AddProjectile(new GenericProjectile(new CollisionBoxInt(Body.Hitbox.X + (this.length / 2), Body.Hitbox.Y + (this.length / 2), GetBulletWidth(),
                            GetBulletHeight()), shootingAngle - 20, this), Body.Hitbox.X + (this.length / 2), Body.Hitbox.Y + (this.length / 2));
                    this.Body.Map.AddProjectile(new GenericProjectile(new CollisionBoxInt(Body.Hitbox.X + (this.length / 2), Body.Hitbox.Y + (this.length / 2), GetBulletWidth(),
                            GetBulletHeight()), shootingAngle - 40, this), Body.Hitbox.X + (this.length / 2), Body.Hitbox.Y + (this.length / 2));
                    this.Body.Map.AddProjectile(new GenericProjectile(new CollisionBoxInt(Body.Hitbox.X + (this.length / 2), Body.Hitbox.Y + (this.length / 2), GetBulletWidth(),
                            GetBulletHeight()), shootingAngle, this), Body.Hitbox.X + (this.length / 2), Body.Hitbox.Y + (this.length / 2));
                }
                catch
                {
                }
            }
            else
            {
                try
                {
                    this.Body.Map.AddProjectile(new GenericProjectile(new CollisionBoxInt(Body.Hitbox.X + this.length, Body.Hitbox.Y + (this.length / 2), GetBulletWidth(),
                            GetBulletHeight()), shootingAngle, this), Body.Hitbox.X + this.length, Body.Hitbox.Y + (this.length / 2));
                    this.Body.Map.AddProjectile(new GenericProjectile(new CollisionBoxInt(Body.Hitbox.X + (this.length / 2), Body.Hitbox.Y + (this.length / 2), GetBulletWidth(),
                            GetBulletHeight()), shootingAngle, this), Body.Hitbox.X + (this.length / 2), Body.Hitbox.Y + (this.length / 2));
                    this.Body.Map.AddProjectile(new GenericProjectile(new CollisionBoxInt(Body.Hitbox.X, Body.Hitbox.Y + (this.length / 2), GetBulletWidth(),
                            GetBulletHeight()), shootingAngle, this), Body.Hitbox.X, Body.Hitbox.Y + (this.length / 2));
                }
                catch
                {
                }
            }
        }

        override protected int GetAngle()
        {
            if (currentAtk == Attacks.barrage || currentAtk == Attacks.bulletHell)
            {
                return PlayerAngle();
            }
            else
            {
                return DIRECTION_DOWN;
            }
        }

        override protected int GetBulletWidth()
        {
            if (this.currentAtk == Attacks.barrage)
            {
                return BARRAGE_WIDTH;
            }
            else if (this.currentAtk == Attacks.bulletHell)
            {
                return BULLETHELL_WIDTH;
            }
            else
            {
                return MITRA_WIDTH;
            }
        }

        override protected int GetBulletHeight()
        {
            if (this.currentAtk == Attacks.barrage)
            {
                return BARRAGE_LENGTH;
            }
            else if (this.currentAtk == Attacks.bulletHell)
            {
                return BULLETHELL_LENGTH;
            }
            else
            {
                return MITRA_LENGTH;
            }
        }

        public enum Attacks
        {
            barrage,
            bulletHell,
            mitra,
        }

    }
}
