﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OOP19_SI.virtualworld;

namespace Barbaro.entity
{
    public interface IVirtualBody
    {
        /**
         * Move the virtualBody if possible and if the movementcontroll is present.
         * @param xvalue Is the X coordinate
         * @param yvalue Is the Y coordinate
         * @return return a boolean representing the action appening
         */
        bool Move(int xvalue, int yvalue);

        /**
         * @return The collisionBox
         */
        CollisionBoxInt Hitbox { get; }

        /**
         * Check the collision between two virtualBody.
         * @param box
         * @return A boolean representing the state
         */
        bool CheckCollision(CollisionBoxInt box);

        /** 
         * @return an optional VirtualMap
         */
        IVirtualMap Map { get; }
    }
}
