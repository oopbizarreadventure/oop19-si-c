﻿using System;
using Bartolucci.formations;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Test
{
    [TestClass]
    public class EnemyFormationTest
    {
        private static IEnemyFormationGenerator generator;
        private static readonly int DEFAUL_WIDTH = 1280;
        private static readonly int DEFAULT_HEIGHT = 720;
        private static readonly int ENEMY_NUMBER_1 = 20;
        private static readonly int ENEMY_NUMBER_2 = 10;

        [TestInitialize]
        public void Init()
        {
            generator = new EnemyFormationGeneratorImpl(DEFAUL_WIDTH, DEFAULT_HEIGHT);
        }

        [TestMethod]
        public void TestFormation()
        {
            IEnemyFormation formation = generator.GetEnemyFormation(ENEMY_NUMBER_1);
            Assert.AreEqual(formation.FormationDictionary.Count, ENEMY_NUMBER_1);
            formation = generator.GetEnemyFormation(ENEMY_NUMBER_2);
            Assert.AreEqual(formation.FormationDictionary.Count, ENEMY_NUMBER_2);
            Console.WriteLine("Formation: \n" + formation.ToString());
        }

        [TestMethod]
        public void TestCoordinates()
        {
            IEnemyFormation formation = generator.GetEnemyFormation(ENEMY_NUMBER_1);
            foreach (var p in formation.FormationDictionary.Keys)
            {
                Assert.IsTrue(p.Key <= DEFAUL_WIDTH);
                Assert.IsTrue(p.Value <= DEFAULT_HEIGHT);
            }
        }

        [TestMethod]
        public void TestBoss()
        {
            IEnemyFormation formation = generator.GetBossWave();
            Assert.IsTrue(formation.FormationDictionary.ContainsValue(EnemyType.BOSS));
            Assert.AreEqual(formation.FormationDictionary.Count, 1);
            Console.WriteLine("Boss: \n" + formation.ToString());
        }
    }
}
