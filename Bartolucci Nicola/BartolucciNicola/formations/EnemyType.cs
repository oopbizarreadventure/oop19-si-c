﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bartolucci.formations
{
    /// <summary>
    /// An enum used to indicate the type of a single enemy that
    /// should be in an EnemyFormation.
    /// </summary>
    public enum EnemyType
    {
        ///<summary>
        /// An Enum value indicating the enemy type "Drone".
        ///</summary>
        DRONE,
        ///<summary>
        /// An Enum value indicating the enemy type "Turret".
        ///</summary>
        TURRET,
        ///<summary>
        /// An Enum value indicating the enemy type "Muncher".
        ///</summary>
        MUNCHER,
        ///<summary>
        /// An Enum value indicating the enemy type "Boss".
        ///</summary>
        BOSS
    }
}
