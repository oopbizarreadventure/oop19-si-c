﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bartolucci.formations
{
    /// <summary>
    /// Basic implementation of the EnemyFormation interface.
    /// </summary>
    public class EnemyFormationImpl : IEnemyFormation
    {
        /// <summary>
        /// A dictionary containing Pairs that indicate the coordinates of the enemy
        /// and EnemyType, the type of enemy with those coordinates.
        /// </summary>
        private readonly Dictionary<KeyValuePair<int, int>, EnemyType> formationDictionary;

        public EnemyFormationImpl(Dictionary<KeyValuePair<int, int>, EnemyType> formationDictionary)
        {
            this.formationDictionary = formationDictionary;
        }

        public Dictionary<KeyValuePair<int, int>, EnemyType> FormationDictionary => formationDictionary;

        public override String ToString()
        {
            int index = 0;
            StringBuilder str = new StringBuilder();
            foreach (var p in formationDictionary.Keys)
            {
                str.Append("Enemy n. " + index + " Type: " + FormationDictionary[p] + " Coordinates: X = " + p.Key
                                        + "; Y = " + p.Value + "\n");
                index++;
            }
            return str.ToString();
        }
    }
}
