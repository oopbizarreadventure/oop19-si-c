﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bartolucci.formations
{
    /// <summary>
    ///This interface provides a single method, used by external
    ///classes to obtain the needed enemy formation.
    /// </summary>
    public interface IEnemyFormationGenerator
    {
        /// <summary>
        /// This method returns a new EnemyFormation.
        /// </summary>
        /// <param name="quantity"> the number of enemies in the wanted EnemyFormation.</param>
        /// <returns> a new EnemyFormation.</returns>
        IEnemyFormation GetEnemyFormation(int quantity);

        /// <summary>
        /// This method returns a new EnemyFormation with only
        /// one entry, whose EnemyType value is BOSS.
        /// </summary>
        /// <returns> a new EnemyFormation with only a BOSS.</returns>
        IEnemyFormation GetBossWave();
    }
}
