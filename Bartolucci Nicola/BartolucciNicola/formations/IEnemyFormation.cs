﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bartolucci.formations
{
    /// <summary>
    /// Interface describing the type EnemyFormation.
    /// </summary>
    public interface IEnemyFormation
    {
        /// <summary>
        /// Returns the Enemy Formation Dictionary, containing coordinates
        /// paired with the type of enemy that should appear
        /// in those coordinates.
        /// </summary>
        /// <returns>the Enemy Formation Dictionary.</returns>
        Dictionary<KeyValuePair<int, int>, EnemyType> FormationDictionary { get; }
    }
}
