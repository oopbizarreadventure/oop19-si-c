﻿using System;
using System.Collections.Generic;

namespace Bartolucci.formations
{
    /// <summary>
    ///An implementation of the EnemyFormationGenerator interface.
    ///This class is able to generate random positions for eachenemy,
    ///as well as randomly choosing an enemy type between the 3 available.
    ///The randomly generated coordinates are then converted
    ///in coordinatesrelative to the VirtualMap's size
    ///and put in an map, used to createan EnemyFormation object.
    /// </summary>
    public class EnemyFormationGeneratorImpl : IEnemyFormationGenerator
    {
        private static readonly int NUM_ROWS = 5;
        private static readonly int NUM_COLUMNS = 5;
        private static readonly int SPAWN_CHANCE_DRONE = 45;
        private static readonly int SPAWN_CHANCE_TURRET = 35;
        private readonly int boxHeight;
        private readonly int boxWidth ;

        /// <summary>
        /// This constructor needs the VirtualMap width and height to set
        /// up the class parameters and calculate the enemies positions
        /// relative to the VirtualMap.
        /// </summary>
        /// <param name="width"> the VirtualMap width.</param>
        /// <param name="height"> the VirtualMap height.</param>
        public EnemyFormationGeneratorImpl(int width, int height)
        {
            boxWidth = width / NUM_COLUMNS;
            boxHeight = height / NUM_ROWS;
        }

        public IEnemyFormation GetEnemyFormation(int quantity)
        {
            var enemyMap = new Dictionary<KeyValuePair<int, int>, EnemyType>();
            var positionList = new List<KeyValuePair<int, int>>();
            for (int i = 0; i < quantity; i++)
            {
                KeyValuePair<int, int> coordinates;
                KeyValuePair<int, int> absolutePosition;
                coordinates = this.generateRandomPosition(positionList);
                absolutePosition = CalculateAbsolutePosition(coordinates);
                positionList.Add(coordinates);
                enemyMap.Add(absolutePosition, this.generateEnemy());
            }
            return new EnemyFormationImpl(enemyMap);
        }

        public IEnemyFormation GetBossWave()
        {
            var enemyMap = new Dictionary<KeyValuePair<int, int>, EnemyType>();
            var coordinates = new KeyValuePair<int, int>(NUM_COLUMNS / 2, NUM_ROWS / 2);
            var absolutePosition = this.CalculateAbsolutePosition(coordinates);
            enemyMap.Add(absolutePosition, EnemyType.BOSS);
            return new EnemyFormationImpl(enemyMap);
        }

        /// <summary>
        /// This method randomly picks an EnemyType out of the 3 available.
        /// Each EnemyType has a different chance to be picked.
        /// </summary>
        /// <returns> a randomly picked EnemyType.</returns>
        private EnemyType generateEnemy()
        {
            Random random = new Random();
            int high = 100;
            int randomizedEnemyNumber = random.Next(high + 1);
            EnemyType enemyType;
            if (randomizedEnemyNumber <= SPAWN_CHANCE_DRONE)
            {
                enemyType = EnemyType.DRONE;
            }
            else if (randomizedEnemyNumber <= SPAWN_CHANCE_DRONE + SPAWN_CHANCE_TURRET)
            {
                enemyType = EnemyType.TURRET;
            }
            else
            {
                enemyType = EnemyType.MUNCHER;
            }
            /*logger.logLine("Randomized enemy generated. It's a " + enemyType.toString() 
                                + ". RNG: " + randomizedEnemyNumber, OutputLevel.LOG);*/
            return enemyType;
        }

        /// <summary>
        /// This method generates random positions.
        /// If the generated position had already been generated previously
        /// (if it was in the positionList), it'll be discarded.
        /// If the position is new, then it is returned
        /// </summary>
        /// <param name="positionList"> the list of already picked positions.</param>
        /// <returns> a new randomly generated position.</returns>
        private KeyValuePair<int, int> generateRandomPosition(List<KeyValuePair<int, int>> positionList)
        {
            int randomizedX;
            int randomizedY;
            Random random = new Random();
            int highX = NUM_COLUMNS;
            int highY = NUM_ROWS - 1;
            KeyValuePair<int, int> coordinates;
            do
            {
                random = new Random();
                randomizedX = random.Next(highX);
                randomizedY = random.Next(highY);
                coordinates = new KeyValuePair<int, int>(randomizedX, randomizedY);
            } while (positionList.Contains(coordinates));
            //logger.logLine("ITERATION. Random coordinates have been added: " + randomizedX + "; " + randomizedY, OutputLevel.LOG);
            return coordinates;
        }

        /// <summary>
        /// This method is used to calculate the coordinates in relation
        /// to the size of the VirtualMap.
        /// </summary>
        /// <param name="relativePosition"> the relative position, based on a 5x4 grid.</param>
        /// <returns> the final coordinates.</returns>
        private KeyValuePair<int, int> CalculateAbsolutePosition(KeyValuePair<int, int> relativePosition)
        {
            KeyValuePair<int, int> boxCenter = BoxCenter;
            int absoluteX = (relativePosition.Key * boxWidth) + boxCenter.Key;
            int absoluteY = (relativePosition.Value * boxHeight) + boxCenter.Value;
            return new KeyValuePair<int, int>(absoluteX, absoluteY);
        }

        /// <summary>
        /// This method is used to calculate the center's 
        /// relative coordinates of a virtual grid box.
        /// </summary>
        /// <returns>a Pair containing the X and Y coordinates,
        /// describing the center of the box.</returns>
        private KeyValuePair<int, int> BoxCenter => new KeyValuePair<int, int>(boxWidth / 2, boxHeight / 2);
    }
}
